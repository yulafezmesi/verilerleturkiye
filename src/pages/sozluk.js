import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import "../style/main.scss"
import GlossaryItems from "../components/glossaryitems"
const IndexPage = () => {
  return (
    <Layout>
      <SEO />
      <main className="p-lg-4 pt-3">
        <GlossaryItems />
      </main>
    </Layout>
  )
}

export default IndexPage
