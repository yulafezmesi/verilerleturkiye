import React, { useEffect, useState, useContext } from "react"
import SEO from "./seo"
import "../style/main.scss"
import { navigate } from "gatsby"
import { MainContext } from "../contexts/maincontext"
const GlossaryItems = ({ id, visibleBottomRef }) => {
  const { glossary } = useContext(MainContext)
  const [glossaryDetail, setGlossaryDetail] = useState({})
  const [linkedTitles, setLinkedTitles] = useState([])

  useEffect(() => {
    const detail = glossary.find(item => item.URLID === id)
    if (!detail) navigate(`/404/`)
    try {
      let titleIdArray = detail.İV.split(",")
      let titles = []
      titleIdArray.map(item => {
        if (detail.ID != item) {
          let title = glossary.find(item2 => item2.ID == item)
          if (title != undefined) {
            titles.push(title)
          }
        }
      })
      setLinkedTitles(titles.sort((a, b) => a.URLID.localeCompare(b.URLID)))
    } catch (e) {}
    setGlossaryDetail(detail)
    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      setTimeout(() => {
        visibleBottomRef.current.scrollIntoView({
          behavior: "smooth",
          block: "start",
          inline: "start",
        })
      }, 800)
    }
  }, [id])
  const htmlToElement = html => {
    var template = document.createElement("template")
    if (html) {
      html = html.trim() // Never return a text node of whitespace as the result
      template.innerHTML = html
      return template.content.firstChild.src.replace("interactive", "image")
    } else {
      return null
    }
  }
  return glossaryDetail ? (
    <>
      <SEO
        meta={glossaryDetail.Metatag}
        description={glossaryDetail.Description}
      />
      <div className="glossary-content-container">
        <h2>{glossaryDetail.Başlık}</h2>
        {/* <div className="d-flex flex-wrap justify-content-between mb-3 ">
          <table>
            {glossaryDetail.YK ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>Yayınlayan Kurum</span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                <td className="glossary-content-container-td">
                  {glossaryDetail.YK}
                </td>
              </tr>
            ) : null}
            {glossaryDetail.İYT ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>
                    İlk Yayınlanma Tarihi
                  </span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                <td className="glossary-content-container-td">
                  {glossaryDetail.İYT}
                </td>
              </tr>
            ) : null}
            {glossaryDetail.P ? (
              <tr>
                <td valign="top" className="glossary-content-container-title">
                  <span style={{ fontSize: "1rem" }}>Periyodu</span>
                  <span className="glossary-content-container-title-dots">
                    :
                  </span>
                </td>
                <td className="glossary-content-container-td">
                  {glossaryDetail.P}
                </td>
              </tr>
            ) : null}
          </table>
          {glossaryDetail.Ö ? (
            <img
              alt={glossaryDetail.B}
              src={`/images/glossary/risk/risk-${glossaryDetail.Ö}.png`}
            />
          ) : null}
        </div> */}
      </div>
      {/* {glossaryDetail.A ? (
        <>
          <h4>Açıklama</h4>
          <p>{glossaryDetail.A}</p>
        </>
      ) : null} */}
      {glossaryDetail.CHURL ? (
        <>
          <h4 className="mb-md-0">Grafik</h4>
          <div className="chart-wrapper-is-mobile">
            <img src={htmlToElement(glossaryDetail.CHURL)} />
          </div>
          <div
            className="chart-wrapper"
            dangerouslySetInnerHTML={{ __html: glossaryDetail.CHURL }}
          ></div>
          {/* {glossaryDetail.GNO ? (
            <>
              <h4 style={{ marginTop: "5.8em " }}>Grafik Nasıl Okunmalı?</h4>
              <p>{glossaryDetail.GNO}</p>
            </>
          ) : null} */}
        </>
      ) : null}
      {/* {glossaryDetail.NNE ? (
        <>
          <h4>Neyi/Nasıl Etkiler?</h4>
          <p>{glossaryDetail.NNE}</p>
        </>
      ) : null} */}
      {/* {glossaryDetail.KŞ ? (
        <>
          <h4>Kullanım Şekli</h4>
          <p>{glossaryDetail.KŞ}</p>
        </>
      ) : null} */}
      {/* {linkedTitles ? (
        <>
          <h4>İlişkili Veriler</h4>
          <ul>
            {linkedTitles.map((item, i) => (
              <li key={item.ID}>
                <a href={`${item.URLID}`}>{item.B}</a>
              </li>
            ))}
          </ul>
        </>
      ) : null} */}
      {/* {glossaryDetail.YTURL ? (
        <div className="embed-container">
          <iframe
            src={glossaryDetail.YTURL}
            frameborder="0"
            allowfullscreen
          ></iframe>
        </div>
      ) : null} */}
    </>
  ) : null
}

export default GlossaryItems
