import React, { createContext, useState, useEffect } from "react"
export const MainContext = createContext()
const MainContextProvider = props => {
  const getJsonArrayFromData = data => {
    var obj = {}
    var result = []
    var headers = data[0]
    var cols = headers.length
    var row = []
    for (var i = 1, l = data.length; i < l; i++) {
      // get a row to fill the object
      row = data[i]
      // clear object
      obj = {}
      for (var col = 0; col < cols; col++) {
        // fill object with new values
        if (!row[col]) {
          row[col] = ""
        } else {
          obj[headers[col]] = row[col]
        }
      }
      // add object in a final result
      result.push(obj)
    }
    return result
  }
  const checkElementIsVisible = el => {
    if (el) {
      var rect = el.getBoundingClientRect()
      var elemTop = rect.top
      var elemBottom = rect.bottom

      // Only completely visible elements return true:
      var isVisible = elemTop >= 0 && elemBottom <= window.innerHeight
      // Partially visible elements return true:
      //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
      return isVisible
    }
  }

  const [offset, setOffset] = useState(0)
  const [pageContent, setPageContent] = useState([])
  const [glossary, setGlossary] = useState([])
  const [isGlossaryLoading, setIsGlossaryLoading] = useState(true)

  const getGlossary = async () => {
    try {
      let data = await fetch(
        "https://sheets.googleapis.com/v4/spreadsheets/1vzi-RR5H6vNAlGF8Ss6eM65WNZnw8IZKnBsl6xgwmEY/values/Kitap!B:Q?majorDimension=ROWS&key=AIzaSyDC_sEIhdm6_okUh3n2pwNXEVy3UTp1D4E"
      )
      let { values } = await data.json()
      if (values.length) {
        setGlossary(getJsonArrayFromData(values))
        setIsGlossaryLoading(false)
        return getJsonArrayFromData(values)
      }
    } catch (e) {
      alert(e)
    }
  }
  useEffect(() => {
    const fetchData = async () => {
      try {
        let data = await fetch(
          "https://sheets.googleapis.com/v4/spreadsheets/1vzi-RR5H6vNAlGF8Ss6eM65WNZnw8IZKnBsl6xgwmEY/values/icerik!A:E?majorDimension=ROWS&key=AIzaSyDC_sEIhdm6_okUh3n2pwNXEVy3UTp1D4E"
        )
        let { values } = await data.json()
        if (values.length) {
          setPageContent(getJsonArrayFromData(values))
        }
      } catch (e) {
        alert(e)
      }
    }
    fetchData()
  }, [])

  return (
    <MainContext.Provider
      value={{
        getGlossary,
        glossary,
        isGlossaryLoading,
      }}
    >
      {props.children}
    </MainContext.Provider>
  )
}
export default MainContextProvider
